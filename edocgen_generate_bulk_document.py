from email import header
import requests
import uuid
import os.path
import json

base_url = "https://app.edocgen.com"
url = "%s/login" % base_url

payload = "{\n\t\"username\":\"tholiya.ravi@gmail.com\",\n\t\"password\": \"password\"\n}"
headers = {
    'content-type': "application/json",
    'cache-control': "no-cache",
    'postman-token': "0ba8be97-557e-fee5-08d6-d05c5eee4710"
    }

response = requests.request("POST", url, data=payload, headers=headers)
x_access_token = response.json()['token']
print ("Using %s for further requests." % x_access_token)

# upload template
url = "%s/api/v1/document" % base_url
headers = {
    'x-access-token': x_access_token
    }

template_file = "/Users/travindra/Documents/name.docx"
files = {'documentFile': open(template_file,'rb')}
values = {}
response = requests.post(url, files=files, data=values, headers=headers)
print(response.json())
template_id = response.json()['id']
print("uploaded document %s with id: %s" %(template_file, template_id))

inputFile = "/Users/travindra/Documents/name.json"

#Generate request
url = "%s/api/v1/document/generate/bulk" % base_url
# the output file extension will be added automatically by the edocgen system
outputFileName = "%s" %uuid.uuid4()
generated_format = "pdf"
inputValues = {
  'documentId': (None, template_id),
  'format': (None, generated_format),
  'filterName': (None, "name"),
  'filterValue': (None, "abc"),
  'outputFileName': (None, outputFileName),
  'inputFile': (os.path.basename(inputFile), open(inputFile, 'rb'), 'application/octet-stream')
}

# headers['content-type'] = 'application/json'
response = requests.post(url, files=inputValues, headers=headers)
print("Generate document: %s" % response.json())
# add extension again due to internal bug
outName = "%s.%s" % (response.json()['outFile'], generated_format)
#Wait for file to get generated and download
url = "%s/api/v1/output/name/%s" %(base_url,  outName)
print("Fetching files with name using url: %s" %url)

response = requests.get(url, headers=headers)
output = response.json()["output"][0] if "output" in response.json() and len(response.json()["output"]) > 0 else None
generated_file_id = None
while(not output):
    print("waiting for file to get generated...", outName)
    response = requests.get(url, headers=headers)
    output = response.json()["output"][0] if "output" in response.json() and len(response.json()["output"]) > 0 else None

generated_file_id = output["_id"]
print("generated file id: %s" % generated_file_id)

# Download the generated file
url = '%s/api/v1/output/download/%s' %(base_url, generated_file_id)
download_to_file = "/tmp/downloaded.%s" % generated_format
response = requests.get(url, headers=headers, allow_redirects=True)
open(download_to_file, 'wb').write(response.content) 
print("File downloaded successfully: %s" % os.path.isfile(download_to_file))

# Delete Template
url = "%s/api/v1/document/%s" %(base_url, template_id)
response = requests.delete(url, headers=headers)
print("Deleted template: %s" %response.json())
# Logout
url = "%s/api/v1/logout" % base_url
response = requests.post(url, headers=headers)
print("Logged out of eDocGen: %s" % response.json())
