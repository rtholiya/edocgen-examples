import requests
import uuid
import os.path
import json


edocgen_dev = 'app.edocgen.com'
base_url = "https://%s" %(edocgen_dev)
url = "%s/login" %(base_url)

payload = "{\n\t\"username\":\"tholiya.ravi@gmail.com\",\n\t\"password\": \"password\"\n}"
headers = {
    'content-type': "application/json",
    'cache-control': "no-cache",
    }

response = requests.request("POST", url, data=payload, headers=headers)
x_access_token = response.json()['token']
print ("Using %s for further requests." % x_access_token)

# upload template
url = "%s/api/v1/document" %(base_url)
headers = {
    'x-access-token': x_access_token
    }

template_file = "/Users/travindra/Downloads/s1.docx"
files = {'documentFile': open(template_file,'rb')}
response = requests.post(url, files=files, headers=headers)
print(response, response.json())
template_id = response.json()["id"]
print("uploaded document %s with id: %s" %(template_file, template_id))

#Generate request
url = "%s/api/v1/document/generate" %(base_url)
# the output file extension will be added automatically by the edocgen system
outputFileName = "abc"
generated_format = "pdf"

template_data = "/Users/travindra/Downloads/s1.json"
inputData = []

with open(template_data, 'r') as content:
	inputData = json.load(content)

print(inputData)


# More than one input will always generate the zip file
inputValues = json.dumps({
  'sync': True,
  'format': generated_format,
  'documentId': template_id,
  'outputFileName': outputFileName,
  'passwordColumn': 'Password',
  'markers': inputData 
})

outFormat = generated_format
if len(inputData) > 1:
	outFormat = 'zip'

headers['content-type'] = 'application/json'
print(url)
response = requests.post(url, data=inputValues, headers=headers, allow_redirects=False)
print(response)
download_to_file = "/Users/travindra/Downloads/downloaded.%s" %(outFormat)
open(download_to_file, 'wb').write(response.content) 
print("File downloaded successfully: %s" % os.path.isfile(download_to_file))

# Delete Template
url = "%s/api/v1/document/%s" %(base_url, template_id)
response = requests.delete(url, headers=headers)
print("Deleted template: %s" %response.json())
# Logout
url = "%s/api/v1/logout" %(base_url)
response = requests.post(url, headers=headers)
print("Logged out of eDocGen: %s" % response.json())
